# aws-kubectl-tkn

Container image based on the official [amazon/aws-cli image](https://github.com/aws/aws-cli/blob/v2/docker/Dockerfile) providing `kubectl` and Tekton CLI `tkn` on top.

### What it's all about

Inside GitLab CI based projects we needed `aws` CLI, `kubectl` and `tkn` available.

As there is no public image available atm we decided to create one ourselves. Since the homebrew based images were way to big (2,5gig), we needed another alternative. We simply used the official AWS CLI container image:

* https://hub.docker.com/r/amazon/aws-cli
* with the Dockerfile https://github.com/aws/aws-cli/blob/v2/docker/Dockerfile (which tells us the image is based on amazonlinux:2)

> why not use `we provided an official GitLab AWS cloud-deploy Docker image` mentioned in this blog: https://about.gitlab.com/blog/2020/12/15/deploy-aws/ ?

Because the mentioned [Dockerfile](https://gitlab.com/gitlab-org/cloud-deploy/-/blob/master/aws/cloud_deploy/Dockerfile) was developed at hacktoberfest, but can't compete against the official amazon/aws-cli image at first look 


### Our GitLab setup: No privileged Docker socket or docker cli

As [a state of the art GitLab setup](https://blog.codecentric.de/en/2021/10/gitlab-ci-paketo-buildpacks/) comprises of non-privileged Kubernetes runners without a mounted or available Docker socket or cli.

So we needed to use Kaniko https://docs.gitlab.com/ee/ci/docker/using_kaniko.html

This means we __must__ write a [Dockerfile](Dockerfile) again.

Installing `kubectl` with `curl` is the easiest way (see https://kubernetes.io/docs/tasks/tools/install-kubectl-linux).

Tekton is installed as described in https://github.com/tektoncd/cli#linux-tarballs.


### Avoid error '/usr/bin/sh: cannot execute binary file'

In order to avoid the error `/usr/bin/sh: cannot execute binary file` inside a `gitlab-ci.yml` using this image, we need to append `"-l", "-c"` to the `ENTRYPOINT` (see https://stackoverflow.com/a/62313159/4964553):

```dockerfile
ENTRYPOINT [ "/bin/bash", "-l", "-c" ]
```
