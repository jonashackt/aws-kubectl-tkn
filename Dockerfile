FROM amazon/aws-cli:2.3.3

ARG TEKTON_VERSION=0.21.0

# see https://kubernetes.io/docs/tasks/tools/install-kubectl-linux
RUN curl -o /usr/local/bin/kubectl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod o+x /usr/local/bin/kubectl

RUN  yum install tar gzip -y \
    && echo "Install jq" \
    && yum install jq -y \
    && echo "Install tkn CLI" \
    && curl -LO "https://github.com/tektoncd/cli/releases/download/v${TEKTON_VERSION}/tkn_${TEKTON_VERSION}_Linux_x86_64.tar.gz" \
    && tar xvzf "tkn_${TEKTON_VERSION}_Linux_x86_64.tar.gz" -C /usr/local/bin/ tkn

# to avoid error '/usr/bin/sh: cannot execute binary file' we need to append "-l", "-c" to the ENTRYPOINT
# see https://stackoverflow.com/a/62313159/4964553
ENTRYPOINT [ "/bin/bash", "-l", "-c" ]